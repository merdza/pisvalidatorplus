﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PisValidatorPlus
{
    public static class PISInvocationHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="arguments"></param>
        public static void Invoke(Delegate handler, params object[] arguments)
        {
            int requiredParameters = handler.Method.GetParameters().Length;

            if (arguments != null)
                if (requiredParameters != arguments.Length)
                    throw new ArgumentException(string.Format("{0} arguments provided when {1} {2} required.", arguments.Length, requiredParameters, ((requiredParameters == 1) ? "is" : "are")));

            Delegate[] invocationList = handler.GetInvocationList();
            if (invocationList == null)
                return;

            foreach (Delegate singleCastDelegate in invocationList)
            {
                ISynchronizeInvoke synchronizeTarget = singleCastDelegate.Target as ISynchronizeInvoke;

                if (synchronizeTarget == null)
                    singleCastDelegate.DynamicInvoke(arguments);
                else
                    synchronizeTarget.BeginInvoke(singleCastDelegate, arguments);
            }
        }
    }
}
