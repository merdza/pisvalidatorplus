﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Windows.Forms;
using System.Timers;
using ITLlib;
using System.Runtime.Serialization;

namespace PisValidatorPlus
{
    public class CValidator
    {
        // ssp library variables
        SSPComms eSSP;
        SSP_COMMAND cmd, storedCmd;
        SSP_KEYS keys;
        SSP_FULL_KEY sspKey;
        SSP_COMMAND_INFO info;

        // variable declarations
        bool generateLogMessages;
        bool generateMainThreadMessages;

        // The comms window class, used to log everything sent to the validator visually and to file
//        CCommsWindow m_Comms; 

        // A variable to hold the type of validator, this variable is initialised using the setup request command
        char m_UnitType;

        // Two variables to hold the number of notes accepted by the validator and the value of those
        // notes when added up
        int m_NumStackedNotes;
        
        // Variable to hold the number of channels in the validator dataset
        int m_NumberOfChannels;

        // The multiplier by which the channel values are multiplied to give their
        // real penny value. E.g. £5.00 on channel 1, the value would be 5 and the multiplier
        // 100.
        int m_ValueMultiplier;

        // A list of dataset data, sorted by value. Holds the info on channel number, value, currency,
        // level and whether it is being recycled.
        List<ChannelData> m_UnitDataList;

        // constructor
        public CValidator()
        {
            eSSP = new SSPComms();
            cmd = new SSP_COMMAND();
            storedCmd = new SSP_COMMAND();
            keys = new SSP_KEYS();
            sspKey = new SSP_FULL_KEY();
            info = new SSP_COMMAND_INFO();
            
//            m_Comms = new CCommsWindow("NoteValidator");
            m_NumberOfChannels = 0;
            m_ValueMultiplier = 1;
            m_UnitType = (char)0xFF;
            m_UnitDataList = new List<ChannelData>();

            generateLogMessages = true;//TODO config.GenerateLogMessages; true za sada
            generateMainThreadMessages = false; //todo
        }

        /* Variable Access */

        // access to ssp variables
        // the pointer which gives access to library functions such as open com port, send command etc
        public SSPComms SSPComms
        {
            get { return eSSP; }
            set { eSSP = value; }
        }

        // a pointer to the command structure, this struct is filled with info and then compiled into
        // a packet by the library and sent to the validator
        public SSP_COMMAND CommandStructure
        {
            get { return cmd; }
            set { cmd = value; }
        }

        public void setCommandStructure(string port, byte addr, uint to)
        {
            CommandStructure.ComPort = port;
            CommandStructure.SSPAddress = addr;
            CommandStructure.Timeout = to;
        }

        public void setEncryptionStatus(bool status)
        {
            CommandStructure.EncryptionStatus = status;
        }

        // pointer to an information structure which accompanies the command structure
        public SSP_COMMAND_INFO InfoStructure
        {
            get { return info; }
            set { info = value; }
        }

        // access to the comms log for recording new log messages
        //public CCommsWindow CommsLog
        //{
        //    get { return m_Comms; }
        //    set { m_Comms = value; }
        //}

        // access to the type of unit, this will only be valid after the setup request
        public char UnitType
        {
            get { return m_UnitType; }
        }

        // access to number of channels being used by the validator
        public int NumberOfChannels
        {
            get { return m_NumberOfChannels; }
            set { m_NumberOfChannels = value; }
        }

        // access to number of notes stacked
        public int NumberOfNotesStacked
        {
            get { return m_NumStackedNotes; }
            set { m_NumStackedNotes = value; }
        }

        // access to value multiplier
        public int Multiplier
        {
            get { return m_ValueMultiplier; }
            set { m_ValueMultiplier = value; }
        }

        // get a channel value
        public int GetChannelValue(int channelNum)
        {
            if (channelNum >= 1 && channelNum <= m_NumberOfChannels)
            {
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNum)
                        return d.Value;
                }
            }
            return -1;
        }

        // get a channel currency
        public string GetChannelCurrency(int channelNum)
        {
            if (channelNum >= 1 && channelNum <= m_NumberOfChannels)
            {
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNum)
                        return new string(d.Currency);
                }
            }
            return "";
        }

        /* Command functions */

        // The enable command allows the validator to receive and act on commands sent to it.
        public void EnableValidator(ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_ENABLE;
            cmd.CommandDataLength = 1;

            if (!SendCommand(ref log)) return;
            // check response
            if (CheckGenericResponses(ref log) && log != null)
                log += @"Unit enabled" + System.Environment.NewLine;
        }

        // Disable command stops the validator from acting on commands.
        public void DisableValidator(ref string log) 
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_DISABLE;
            cmd.CommandDataLength = 1;

            if (!SendCommand(ref log)) return;
            // check response
            if (CheckGenericResponses(ref log) && log != null)
                log += @"Unit disabled" + System.Environment.NewLine;
        }

        // The reset command instructs the validator to restart (same effect as switching on and off)
        public void Reset(ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_RESET;
            cmd.CommandDataLength = 1;
            if (!SendCommand(ref log)) return;

            if(CheckGenericResponses(ref log))
                log += @"Resetting unit" + System.Environment.NewLine;
        }

        // This command just sends a sync command to the validator
        public bool SendSync(ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_SYNC;
            cmd.CommandDataLength = 1;
            if (!SendCommand(ref log)) return false;

            if (CheckGenericResponses(ref log))
                log += @"Successfully sent sync" + System.Environment.NewLine;
            return true;
        }

        // This function sets the protocol version in the validator to the version passed across. Whoever calls
        // this needs to check the response to make sure the version is supported.
        public void SetProtocolVersion(byte pVersion, ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_HOST_PROTOCOL_VERSION;
            cmd.CommandData[1] = pVersion;
            cmd.CommandDataLength = 2;
            if (!SendCommand(ref log)) return;
        }

        public byte getResponseData()
        {
            return CommandStructure.ResponseData[0];
        }

        // This function sends the command LAST REJECT CODE which gives info about why a note has been rejected. It then
        // outputs the info to a passed across textbox.
        public void QueryRejection(ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_LAST_REJECT_CODE;
            cmd.CommandDataLength = 1;
            if (!SendCommand(ref log)) return;

            if (CheckGenericResponses(ref log))
            {
                if (log == null) return;

                log += @"Reason for rejection: ";

                switch (cmd.ResponseData[1])
                {
                    case 0x00: log+=@"Note accepted"; break;
                    case 0x01: log+=@"Note length incorrect"; break;
                    case 0x02: log+=@"Invalid note"; break;
                    case 0x03: log+=@"Invalid note"; break;
                    case 0x04: log+=@"Invalid note"; break;
                    case 0x05: log+=@"Invalid note"; break;
                    case 0x06: log+=@"Channel inhibited"; break;
                    case 0x07: log+=@"Second note inserted during read"; break;
                    case 0x08: log+=@"Host rejected note"; break;
                    case 0x09: log+=@"Invalid note"; break;
                    case 0x0A: log+=@"Invalid note read"; break;
                    case 0x0B: log+=@"Note too long"; break;
                    case 0x0C: log+=@"Validator disabled"; break;
                    case 0x0D: log+=@"Mechanism slow/stalled"; break;
                    case 0x0E: log+=@"Strim attempt"; break;
                    case 0x0F: log+=@"Fraud channel reject"; break;
                    case 0x10: log+=@"No notes inserted"; break;
                    case 0x11: log+=@"Invalid note read"; break;
                    case 0x12: log+=@"Twisted note detected"; break;
                    case 0x13: log+=@"Escrow time-out"; break;
                    case 0x14: log+=@"Bar code scan fail"; break;
                    case 0x15: log+=@"Invalid note read"; break;
                    case 0x16: log+=@"Invalid note read"; break;
                    case 0x17: log+=@"Invalid note read"; break;
                    case 0x18: log+=@"Invalid note read"; break;
                    case 0x19: log+=@"Incorrect note width"; break;
                    case 0x1A: log+=@"Note too short"; break;
                    default: log+=@"Unknown error";break;
                }
                log += @" " + cmd.ResponseData[1].ToString();
            }
        }

        // This function performs a number of commands in order to setup the encryption between the host and the validator.
        public bool NegotiateKeys(ref string log)
        {
            byte i;

            // make sure encryption is off
            cmd.EncryptionStatus = false;

            // send sync
            if (log != null) log+=@"Syncing... ";
            cmd.CommandData[0] = CCommands.SSP_CMD_SYNC;
            cmd.CommandDataLength = 1;

            if (!SendCommand(ref log)) return false;
            if (log != null) log+=@"Success";

            eSSP.InitiateSSPHostKeys(keys, cmd);

            // send generator
            cmd.CommandData[0] = CCommands.SSP_CMD_SET_GENERATOR;
            cmd.CommandDataLength = 9;
            if (log != null) log+=@"Setting generator... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.Generator >> (8 * i));
            }

            if (!SendCommand(ref log)) return false;
            if (log != null) log += @"Success" + System.Environment.NewLine;

            // send modulus
            cmd.CommandData[0] = CCommands.SSP_CMD_SET_MODULUS;
            cmd.CommandDataLength = 9;
            if (log != null) log+=@"Sending modulus... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.Modulus >> (8 * i));
            }

            if (!SendCommand(ref log)) return false;
            if (log != null) log += @"Success" + System.Environment.NewLine;

            // send key exchange
            cmd.CommandData[0] = CCommands.SSP_CMD_KEY_EXCHANGE;
            cmd.CommandDataLength = 9;
            if (log != null) log+=@"Exchanging keys... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.HostInter >> (8 * i));
            }

            if (!SendCommand(ref log)) return false;
            if (log != null) log += @"Success" + System.Environment.NewLine;

            keys.SlaveInterKey = 0;
            for (i = 0; i < 8; i++)
            {
                keys.SlaveInterKey += (UInt64)cmd.ResponseData[1 + i] << (8 * i);
            }

            eSSP.CreateSSPHostEncryptionKey(keys);

            // get full encryption key
            cmd.Key.FixedKey = 0x0123456701234567;
            cmd.Key.VariableKey = keys.KeyHost;

            if (log != null) log += @"Keys successfully negotiated" + System.Environment.NewLine;

            return true;
        }

        // This function uses the setup request command to get all the information about the validator.
        public void SetupRequest(ref string log)
        {
            // send setup request
            cmd.CommandData[0] = CCommands.SSP_CMD_SETUP_REQUEST;
            cmd.CommandDataLength = 1;

            if (!SendCommand(ref log)) return;

            // display setup request
            string displayString = "Unit Type: ";
            int index = 1;

            // unit type (table 0-1)
            m_UnitType = (char)cmd.ResponseData[index++];
            switch (m_UnitType)
            {
                case (char)0x00: displayString += "Validator"; break;
                case (char)0x03: displayString += "SMART Hopper"; break;
                case (char)0x06: displayString += "SMART Payout"; break;
                case (char)0x07: displayString += "NV11"; break;
                default: displayString += "Unknown Type"; break;
            }

            displayString += System.Environment.NewLine + "Firmware: ";

            // firmware (table 2-5)
            while (index <= 5)
            {
                displayString += (char)cmd.ResponseData[index++];
                if (index == 4)
                    displayString += ".";
            }

            // country code (table 6-8)
            // this is legacy code, in protocol version 6+ each channel has a seperate currency
            index = 9; // to skip country code

            // value multiplier (table 9-11) 
            // also legacy code, a real value multiplier appears later in the response
            index = 12; // to skip value multiplier

            displayString += System.Environment.NewLine + "Number of Channels: ";
            int numChannels = cmd.ResponseData[index++];
            m_NumberOfChannels = numChannels;

            displayString += numChannels + System.Environment.NewLine;
            // channel values (table 13 to 13+n)
            // the channel values located here in the table are legacy, protocol 6+ provides a set of expanded
            // channel values.
            index = 13 + m_NumberOfChannels; // Skip channel values

            // channel security (table 13+n to 13+(n*2))
            // channel security values are also legacy code
            index = 13 + (m_NumberOfChannels * 2); // Skip channel security

            displayString += "Real Value Multiplier: ";

            // real value multiplier (table 13+(n*2) to 15+(n*2))
            // (big endian)
            m_ValueMultiplier = cmd.ResponseData[index + 2];
            m_ValueMultiplier += cmd.ResponseData[index + 1] << 8;
            m_ValueMultiplier += cmd.ResponseData[index] << 16;
            displayString += m_ValueMultiplier + System.Environment.NewLine + "Protocol Version: ";
            index += 3;

            // protocol version (table 16+(n*2))
            index = 16 + (m_NumberOfChannels * 2);
            int protocol = cmd.ResponseData[index++];
            displayString += protocol + System.Environment.NewLine;

            // protocol 6+ only

            // channel currency country code (table 17+(n*2) to 17+(n*5))
            index = 17 + (m_NumberOfChannels * 2);
            int sectionEnd = 17 + (m_NumberOfChannels * 5);
            int count = 0;
            byte[] channelCurrencyTemp = new byte[3 * m_NumberOfChannels];
            while (index < sectionEnd)
            {
                displayString += "Channel " + ((count / 3) + 1) + ", currency: ";
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                displayString += System.Environment.NewLine;
            }

            // expanded channel values (table 17+(n*5) to 17+(n*9))
            index = sectionEnd;
            displayString += "Expanded channel values:" + System.Environment.NewLine;
            sectionEnd = 17 + (m_NumberOfChannels * 9);
            int n = 0;
            count = 0;
            int[] channelValuesTemp = new int[m_NumberOfChannels];
            while (index < sectionEnd)
            {
                n = CHelpers.ConvertBytesToInt32(cmd.ResponseData, index);
                channelValuesTemp[count] = n;
                index += 4;
                displayString += "Channel " + ++count + ", value = " + n + System.Environment.NewLine;
            }

            // Create list entry for each channel
            m_UnitDataList.Clear(); // clear old table
            for (byte i = 0; i < m_NumberOfChannels; i++)
            {
                ChannelData d = new ChannelData();
                d.Channel = i;
                d.Channel++; // Offset from array index by 1
                d.Value = channelValuesTemp[i] * Multiplier;
                d.Currency[0] = (char)channelCurrencyTemp[0 + (i * 3)];
                d.Currency[1] = (char)channelCurrencyTemp[1 + (i * 3)];
                d.Currency[2] = (char)channelCurrencyTemp[2 + (i * 3)];
                d.Level = 0; // Can't store notes 
                d.Recycling = false; // Can't recycle notes

                m_UnitDataList.Add(d);
            }

            // Sort the list of data by the value.
            m_UnitDataList.Sort(delegate(ChannelData d1, ChannelData d2) { return d1.Value.CompareTo(d2.Value); });

            if (log != null)
                log+=@displayString;
        }

        // This function sends the set inhibits command to set the inhibits on the validator. An additional two
        // bytes are sent along with the command byte to indicate the status of the inhibits on the channels.
        // For example 0xFF and 0xFF in binary is 11111111 11111111. This indicates all 16 channels supported by
        // the validator are uninhibited. If a user wants to inhibit channels 8-16, they would send 0x00 and 0xFF.
        public void SetInhibits(ref string log)
        {
            // set inhibits
            cmd.CommandData[0] = CCommands.SSP_CMD_SET_INHIBITS;
            cmd.CommandData[1] = 0xFF;
            cmd.CommandData[2] = 0xFF;
            cmd.CommandDataLength = 3;

            if (!SendCommand(ref log)) return;
            if (CheckGenericResponses(ref log) && log != null)
            {
                log += @"Inhibits set" + System.Environment.NewLine;
            }
        }

        public bool reject(ref string log)
        {
            cmd.CommandData[0] = CCommands.SSP_CMD_REJECT;
            cmd.CommandDataLength = 1;

            if (!SendCommand(ref log)) return false;
            return true;
        }

        // The poll function is called repeatedly to poll to validator for information, it returns as
        // a response in the command structure what events are currently happening.
        public bool DoPoll(ref string log)
        {
            byte i;
            int chanel;
            //send poll
            cmd.CommandData[0] = CCommands.SSP_CMD_POLL;
            cmd.CommandDataLength = 1;

            bool sendCommandSuccessful = SendCommand(ref log); 
            if (!sendCommandSuccessful)
            {
                return false;
            }

            //parse poll response
            for (i = 1; i < cmd.ResponseDataLength; i++)
            {
                switch (cmd.ResponseData[i])
                {
                    // This response indicates that the unit was reset and this is the first time a poll has been called since the reset.
                    case CCommands.SSP_POLL_RESET:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnReset), null);
                        break;
                    // A note is currently being read by the validator sensors. The second byte of this response is zero until the note's type has been determined, it then changes to the channel of the scanned note.
                    case CCommands.SSP_POLL_NOTE_READ:
                        chanel = cmd.ResponseData[i + 1];
                        if (chanel > 0)
                        {
                            PISInvocationHelper.Invoke(new DelegateInt(OnReadNote), chanel);
                        }
                        i++;
                        break;
                    // A credit event has been detected, this is when the validator has accepted a note as legal currency.
                    case CCommands.SSP_POLL_CREDIT:
                        chanel = cmd.ResponseData[i + 1];
                        PISInvocationHelper.Invoke(new DelegateInt(OnCreditNote), chanel);
                        i++;
                        break;
                    // A note is being rejected from the validator. This will carry on polling while the note is in transit.
                    case CCommands.SSP_POLL_REJECTING:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnRejecting), null);
                        break;
                    // A note has been rejected from the validator, the note will be resting in the bezel. This response only appears once.
                    case CCommands.SSP_POLL_REJECTED:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnRejected), null);
                        QueryRejection(ref log);
                        break;
                    // A note is in transit to the cashbox.
                    case CCommands.SSP_POLL_STACKING:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnStacking), null);
                        break;
                    // A note has reached the cashbox.
                    case CCommands.SSP_POLL_STACKED:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnStacked), null);
                        break;
                    // A safe jam has been detected. This is where the user has inserted a note and the note is jammed somewhere that the user cannot reach.
                    case CCommands.SSP_POLL_SAFE_JAM:
                        PISInvocationHelper.Invoke(new DelegateString(OnLog), GetLogText(LogTextType.INFO, "SAFE_JAM"));
                        break;
                    // An unsafe jam has been detected. This is where a user has inserted a note and the note is jammed somewhere that the user can potentially recover the note from.
                    case CCommands.SSP_POLL_UNSAFE_JAM:
                        PISInvocationHelper.Invoke(new DelegateString(OnLog), GetLogText(LogTextType.INFO, "UNSAFE_JAM"));
                        break;
                    // The validator is disabled, it will not execute any commands or do any actions until enabled.
                    case CCommands.SSP_POLL_DISABLED:
                        break;
                    // A fraud attempt has been detected. The second byte indicates the channel of the note that a fraud has been attempted on.
                    case CCommands.SSP_POLL_FRAUD_ATTEMPT:
                        PISInvocationHelper.Invoke(new DelegateString(OnLog), GetLogText(LogTextType.INFO, "FRAUD_ATTEMPT"));
                        i++;
                        break;
                    // The stacker (cashbox) is full. 
                    case CCommands.SSP_POLL_STACKER_FULL:
                        PISInvocationHelper.Invoke(new DelegateVoid(OnStackerFull), null);
                        break;
                    // A note was detected somewhere inside the validator on startup and was rejected from the front of the unit.
                    case CCommands.SSP_POLL_NOTE_CLEARED_FROM_FRONT:
                        log += @GetChannelValue(cmd.ResponseData[i + 1]) + " note cleared from front at reset." + System.Environment.NewLine;
                        i++;
                        break;
                    // A note was detected somewhere inside the validator on startup and was cleared into the cashbox.
                    case CCommands.SSP_POLL_NOTE_CLEARED_TO_CASHBOX:
                        log += @GetChannelValue(cmd.ResponseData[i + 1]) + " note cleared to stacker at reset." + System.Environment.NewLine;
                        i++;
                        break;
                    // The cashbox has been removed from the unit. This will continue to poll until the cashbox is replaced.
                    case CCommands.SSP_POLL_CASHBOX_REMOVED:
                        log += @"Cashbox removed..." + System.Environment.NewLine;
                        break;
                    // The cashbox has been replaced, this will only display on a poll once.
                    case CCommands.SSP_POLL_CASHBOX_REPLACED:
                        log += @"Cashbox replaced" + System.Environment.NewLine;
                        break;
                    // A bar code ticket has been detected and validated. The ticket is in escrow, continuing to poll will accept
                    // the ticket, sending a reject command will reject the ticket.
                    case CCommands.SSP_POLL_BAR_CODE_VALIDATED:
                        log += @"Bar code ticket validated" + System.Environment.NewLine;
                        break;
                    // A bar code ticket has been accepted (equivalent to note credit).
                    case CCommands.SSP_POLL_BAR_CODE_ACK:
                        log += @"Bar code ticket accepted" + System.Environment.NewLine;
                        break;
                    // The validator has detected its note path is open. The unit is disabled while the note path is open.
                    // Only available in protocol versions 6 and above.
                    case CCommands.SSP_POLL_NOTE_PATH_OPEN:
                        log += @"Note path open" + System.Environment.NewLine;
                        break;
                    // All channels on the validator have been inhibited so the validator is disabled. Only available on protocol versions 7 and above.
                    case CCommands.SSP_POLL_CHANNEL_DISABLE:
                        log += @"All channels inhibited, unit disabled" + System.Environment.NewLine;
                        break;
                    default:
                        log += @"Unrecognised poll response detected " + (int)cmd.ResponseData[i] + System.Environment.NewLine;
                        break;
                }
            }
            return true;
        }

        /* Non-Command functions */

        // This function calls the open com port function of the SSP library.
        public bool OpenComPort(ref string log)
        {
            if (log != null)
                log += @"Opening com port" + System.Environment.NewLine;
            if (!eSSP.OpenSSPComPort(cmd))
            {
                return false;
            }
            return true;
        }

        /* Exception and Error Handling */

        // This is used for generic response error catching, it outputs the info in a
        // meaningful way.
        private bool CheckGenericResponses(ref string log)
        {
            if (cmd.ResponseData[0] == CCommands.SSP_RESPONSE_CMD_OK)
                return true;
            else
            {
                if (log != null)
                {
                    switch (cmd.ResponseData[0])
                    {
                        case CCommands.SSP_RESPONSE_CMD_CANNOT_PROCESS:
                            if (cmd.ResponseData[1] == 0x03)
                            {
                                log += @"Validator has responded with ""Busy"", command cannot be processed at this time" + System.Environment.NewLine;
                            }
                            else
                            {
                                log+=@"Command response is CANNOT PROCESS COMMAND, error code - 0x"
                                + BitConverter.ToString(cmd.ResponseData, 1, 1) + System.Environment.NewLine;
                            }
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_FAIL:
                            log += @"Command response is FAIL" + System.Environment.NewLine;
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_KEY_NOT_SET:
                            log+=@"Command response is KEY NOT SET, Validator requires encryption on this command or there is"
                                + "a problem with the encryption on this request" + System.Environment.NewLine;
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_PARAM_OUT_OF_RANGE:
                            log += @"Command response is PARAM OUT OF RANGE" + System.Environment.NewLine;
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_SOFTWARE_ERROR:
                            log += @"Command response is SOFTWARE ERROR" + System.Environment.NewLine;
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_UNKNOWN:
                            log += @"Command response is UNKNOWN" + System.Environment.NewLine;
                            return false;
                        case CCommands.SSP_RESPONSE_CMD_WRONG_PARAMS:
                            log += @"Command response is WRONG PARAMETERS" + System.Environment.NewLine;
                            return false;
                        default:
                            return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool SendCommand(ref string log)
        {
            // Backup data and length in case we need to retry
            byte[] backup = new byte[255];
            cmd.CommandData.CopyTo(backup, 0);
            byte length = cmd.CommandDataLength;

            // attempt to send the command
            if (eSSP.SSPSendCommand(cmd, info) == false)
            {
                eSSP.CloseComPort();
//                m_Comms.UpdateLog(info, true); // update the log on fail as well
                if (log != null) log += @"Sending command failed" + System.Environment.NewLine + "Port status: " + cmd.ResponseStatus.ToString() + System.Environment.NewLine;
                return false;
            }

            // update the log after every command
//            m_Comms.UpdateLog(info);

            return true;
        }

        public void closeComPort()
        {
            SSPComms.CloseComPort();
        }

        String GetLogText(LogTextType textType, String text)
        {
            //return String.Format("[CardNum: {2}] [{0}] {1}", Enum.GetName(typeof(LogTextType), textType), text, CardNumber);
            return String.Format("[{0}] {1}", Enum.GetName(typeof(LogTextType), textType), text);
        }

        enum LogTextType
        {
            ERROR,
            INFO
        }

        #region Delagates

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        delegate void DelegateInt(int param);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        delegate void DelegateString(String param);

        /// <summary>
        /// 
        /// </summary>
        delegate void DelegateVoid();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        delegate void DelegateException(Exception ex);

        #endregion

        #region Event Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        protected virtual void OnException(Exception ex)
        {
            EventHandler<ExceptionEventArgs> localHandler = ExceptionEvent;
            if (localHandler != null)
                localHandler(this, new ExceptionEventArgs(ex));
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnReset()
        {
            //PISInvocationHelper.Invoke(new DelegateString(OnLog), GetLogText(LogTextType.INFO, "VALIDATOR_RESET"));

            EventHandler<ResetEventArgs> localHandler = ResetEvent;
            if (localHandler != null)
                localHandler(this, new ResetEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnDisabled()
        {
            //PISInvocationHelper.Invoke(new DelegateString(OnLog), GetLogText(LogTextType.INFO, "VALIDATOR_DISABLED"));

            EventHandler<DisabledEventArgs> localHandler = DisabledEvent;
            if (localHandler != null)
                localHandler(this, new DisabledEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chanel"></param>
        protected virtual void OnReadNote(int chanel)
        {
            //OnLog(GetLogText(LogTextType.INFO, String.Format("VALIDATOR_NOTE_READ at Chanel: {0} Value:{1}", chanel - 1, chanel == 0 ? 0 : ChanelValue[chanel - 1] * ValueMultiplier)));
            OnLog(GetLogText(LogTextType.INFO, String.Format("VALIDATOR_NOTE_READ at Chanel: {0} Value:{1}", chanel, chanel == 0 ? 0 : GetChannelValue(chanel))));
            EventHandler<ReadNoteEventArgs> localHandler = ReadNoteEvent;
            if (localHandler != null)
                localHandler(this, new ReadNoteEventArgs(chanel));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chanel"></param>
        protected virtual void OnCreditNote(int chanel)
        {
            //OnLog(GetLogText(LogTextType.INFO, String.Format("VALIDATOR_NOTE_CREDIT Chanel: {0} Value:{1}", chanel - 1, ChanelValue[chanel - 1] * ValueMultiplier)));
            OnLog(GetLogText(LogTextType.INFO, String.Format("VALIDATOR_NOTE_CREDIT at Chanel: {0} Value:{1}", chanel, chanel == 0 ? 0 : GetChannelValue(chanel))));
            EventHandler<CreditNoteEventArgs> localHandler = CreditNoteEvent;
            if (localHandler != null)
                localHandler(this, new CreditNoteEventArgs(chanel));
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnRejecting()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_REJECTING"));

            EventHandler<RejectingEventArgs> localHandler = RejectingEvent;
            if (localHandler != null)
                localHandler(this, new RejectingEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnRejected()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_REJECTED"));
            EventHandler<RejectedEventArgs> localHandler = RejectedEvent;
            if (localHandler != null)
                localHandler(this, new RejectedEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnStacking()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_STACKING"));
            EventHandler<StackingEventArgs> localHandler = StackingEvent;
            if (localHandler != null)
                localHandler(this, new StackingEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnStacked()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_STACKED"));
            EventHandler<StackedEventArgs> localHandler = StackedEvent;
            if (localHandler != null)
                localHandler(this, new StackedEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnStackerFull()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_STACKER_FULL"));
            EventHandler<StackerFullEventArgs> localHandler = StackerFullEvent;
            if (localHandler != null)
                localHandler(this, new StackerFullEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void OnStoredNote()
        {
            OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_NOTE_STORED"));
            EventHandler<StoredNoteEventArgs> localHandler = StoredNoteEvent;
            if (localHandler != null)
                localHandler(this, new StoredNoteEventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        protected virtual void OnLog(String text)
        {
            //Console.WriteLine(text);
            EventHandler<LogEventArgs> localHandler = LogEvent;
            if (localHandler != null)
                if (!generateLogMessages)
                    throw new ItlSspErrorInitConfigException("Generating Log Messages is disabled!");
                else
                    localHandler(this, new LogEventArgs(text));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        protected virtual void OnLogMainThread(String text)
        {
            EventHandler<LogEventArgs> localHandler = LogEvent;
            if (localHandler != null)
                if (!generateLogMessages)
                    throw new ItlSspErrorGetLogMessageException("Generating Log Messages is disabled!");
                else
                    if (generateMainThreadMessages)
                        localHandler(this, new LogEventArgs(text));
        }

        /// <summary>
        /// 
        /// </summary>
        //protected virtual void OnValidatorInit()
        //{
        //    //OnLog(GetLogText(LogTextType.INFO, "VALIDATOR_INIT"));
        //    EventHandler<ValidatorInitEventArgs> localHandler = ValidatorInitEvent;
        //    if (localHandler != null)
        //        localHandler(this, new ValidatorInitEventArgs(CountyCode, NumberOfChannels, ChanelValue.ToArray(), SerialNumber));
        //}

        #endregion

        #region Event Handlers

        public event EventHandler<ExceptionEventArgs> ExceptionEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<ResetEventArgs> ResetEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<DisabledEventArgs> DisabledEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<ReadNoteEventArgs> ReadNoteEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<CreditNoteEventArgs> CreditNoteEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<RejectingEventArgs> RejectingEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<RejectedEventArgs> RejectedEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<StackingEventArgs> StackingEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<StackedEventArgs> StackedEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<StackerFullEventArgs> StackerFullEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<StoredNoteEventArgs> StoredNoteEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler<LogEventArgs> LogEvent;


        #endregion
    };

    #region Validator Event Args Definitions

    public class ExceptionEventArgs : EventArgs
    {
        public Exception Exception { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        public ExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CreditNoteEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public int Chanel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chanel"></param>
        public CreditNoteEventArgs(int chanel)
        {
            Chanel = chanel;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReadNoteEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public int Chanel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chanel"></param>
        public ReadNoteEventArgs(int chanel)
        {
            Chanel = chanel;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ResetEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public ResetEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DisabledEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public DisabledEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RejectingEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public RejectingEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RejectedEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public RejectedEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StackingEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public StackingEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StackedEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public StackedEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StackerFullEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public StackerFullEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StoredNoteEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public StoredNoteEventArgs()
        {
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LogEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        public LogEventArgs(String text)
        {
            Text = String.Format("{0} ", text);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ValidatorInitEventArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int NumberOfChanels { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int[] ChanelValues { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ValidatorSerialNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="numberOfChanels"></param>
        /// <param name="chanelValues"></param>
        /// <param name="validatorSerialnumber"></param>
        public ValidatorInitEventArgs(String countryCode, int numberOfChanels, long[] chanelValues, long validatorSerialnumber)
        {
            CountryCode = countryCode;
            ValidatorSerialNumber = (int)validatorSerialnumber;
            NumberOfChanels = numberOfChanels;
            ChanelValues = new int[chanelValues.Length];
            for (int i = 0; i < chanelValues.Length; i++)
                ChanelValues[i] = (int)chanelValues[i];
        }
    }

    #endregion

    #region ITLSSP Exceptions

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspException : Exception
    {
        public ItlSspException() { }
        public ItlSspException(string message) : base(message) { }
        public ItlSspException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorOpenPortException : ItlSspException
    {
        public ItlSspErrorOpenPortException() { }
        public ItlSspErrorOpenPortException(string message) : base(message) { }
        public ItlSspErrorOpenPortException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorOpenPortException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorCmdSynchException : ItlSspException
    {
        public ItlSspErrorCmdSynchException() { }
        public ItlSspErrorCmdSynchException(string message) : base(message) { }
        public ItlSspErrorCmdSynchException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorCmdSynchException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorPortClosedException : ItlSspException
    {
        public ItlSspErrorPortClosedException() { }
        public ItlSspErrorPortClosedException(string message) : base(message) { }
        public ItlSspErrorPortClosedException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorPortClosedException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorPortErrorException : ItlSspException
    {
        public ItlSspErrorPortErrorException() { }
        public ItlSspErrorPortErrorException(string message) : base(message) { }
        public ItlSspErrorPortErrorException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorPortErrorException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspCmdTimeoutException : ItlSspException
    {
        public ItlSspErrorSspCmdTimeoutException() { }
        public ItlSspErrorSspCmdTimeoutException(string message) : base(message) { }
        public ItlSspErrorSspCmdTimeoutException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspCmdTimeoutException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorPortOpenException : ItlSspException
    {
        public ItlSspErrorPortOpenException() { }
        public ItlSspErrorPortOpenException(string message) : base(message) { }
        public ItlSspErrorPortOpenException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorPortOpenException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspPacketErrorException : ItlSspException
    {
        public ItlSspErrorSspPacketErrorException() { }
        public ItlSspErrorSspPacketErrorException(string message) : base(message) { }
        public ItlSspErrorSspPacketErrorException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspPacketErrorException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspReplayOkException : ItlSspException
    {
        public ItlSspErrorSspReplayOkException() { }
        public ItlSspErrorSspReplayOkException(string message) : base(message) { }
        public ItlSspErrorSspReplayOkException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspReplayOkException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class ItlSspErrorSspResetException : ItlSspException
    {
        public ItlSspErrorSspResetException() { }
        public ItlSspErrorSspResetException(string message) : base(message) { }
        public ItlSspErrorSspResetException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspResetException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorResponseSspException : ItlSspException
    {
        public ItlSspErrorResponseSspException() { }
        public ItlSspErrorResponseSspException(string message) : base(message) { }
        public ItlSspErrorResponseSspException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorResponseSspException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorGetSerialNumberException : ItlSspException
    {
        public ItlSspErrorGetSerialNumberException() { }
        public ItlSspErrorGetSerialNumberException(string message) : base(message) { }
        public ItlSspErrorGetSerialNumberException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorGetSerialNumberException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSetupRequestException : ItlSspException
    {
        public ItlSspErrorSetupRequestException() { }
        public ItlSspErrorSetupRequestException(string message) : base(message) { }
        public ItlSspErrorSetupRequestException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSetupRequestException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspRejectException : ItlSspException
    {
        public ItlSspErrorSspRejectException() { }
        public ItlSspErrorSspRejectException(string message) : base(message) { }
        public ItlSspErrorSspRejectException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspRejectException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspPollException : ItlSspException
    {
        public ItlSspErrorSspPollException() { }
        public ItlSspErrorSspPollException(string message) : base(message) { }
        public ItlSspErrorSspPollException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspPollException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspSetInhibitsException : ItlSspException
    {
        public ItlSspErrorSspSetInhibitsException() { }
        public ItlSspErrorSspSetInhibitsException(string message) : base(message) { }
        public ItlSspErrorSspSetInhibitsException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspSetInhibitsException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspEnableException : ItlSspException
    {
        public ItlSspErrorSspEnableException() { }
        public ItlSspErrorSspEnableException(string message) : base(message) { }
        public ItlSspErrorSspEnableException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspEnableException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorInitConfigException : ItlSspException
    {
        public ItlSspErrorInitConfigException() { }
        public ItlSspErrorInitConfigException(string message) : base(message) { }
        public ItlSspErrorInitConfigException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorInitConfigException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorSspDisableException : ItlSspException
    {
        public ItlSspErrorSspDisableException() { }
        public ItlSspErrorSspDisableException(string message) : base(message) { }
        public ItlSspErrorSspDisableException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorSspDisableException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErrorGetLogMessageException : ItlSspException
    {
        public ItlSspErrorGetLogMessageException() { }
        public ItlSspErrorGetLogMessageException(string message) : base(message) { }
        public ItlSspErrorGetLogMessageException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErrorGetLogMessageException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ItlSspErorConfigException : ItlSspException
    {
        public ItlSspErorConfigException() { }
        public ItlSspErorConfigException(string message) : base(message) { }
        public ItlSspErorConfigException(string message, Exception inner) : base(message, inner) { }
        protected ItlSspErorConfigException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context) { }
    }

    #endregion
}
